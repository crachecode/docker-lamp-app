color_method = \033[93m

-include .env
export
containers = ${APP_NAME}_db ${APP_NAME} ${APP_NAME}_adm
selfiles = $(wildcard .[!.]*.tpl *.tpl) install.sh .git .gitignore README.md LICENSE

help: ## Display this help message
	@printf "\n$(color_method)help\033[0m\n\n"
	@awk -F ':|##' '/^[^\t].+?:.*?##/ {\
		printf "\033[36m%-30s\033[0m %s\n", $$1, $$NF \
	}' $(MAKEFILE_LIST) | sort
	@printf "\n"

install: install.sh
	@chmod +w install.sh
	@chmod +x *.sh
	@./install.sh
	@mkdir -p ./app
	@rm -rf $(selfiles)
	@echo "LAMP has been installed.\nPlace your files in the \033[1;32mapp\e[0m directory.\nThe next thing you want is probably to run \033[1;32mmake database\e[0m to create a database or import yours.\nInstall Pipe Viewer (PV) to get a nice progress bar.\nType \033[1;32mmake\e[0m to list every commands.\n"

database: start ## Import a database from a SQL file or create a new.
	@read -p "Do you want to import an existing database ? [y/N] " ans && ans=$${ans:-N} ; \
	if [ $${ans} = y ] || [ $${ans} = Y ]; then \
		echo "Importing database..."; \
    ./import-db.sh; \
  else \
		echo "Creating database..."; \
    ./create-db.sh; \
  fi

start: ## Start the app
	@if [ -z "$(shell docker network ls -q -f name=web)" ]; then \
		echo "create web network"; \
		docker network create web; \
	fi;
	@if [ ! -z "$(shell docker ps -q -f name=traefik)" ]; then \
		echo "traefik is running, starting $(containers)"; \
		docker compose up -d $(containers); \
	elif [ ! -z "$(shell docker ps -qa -f name=traefik)" ]; then \
		echo "traefik is stopped, starting both"; \
		docker start traefik; \
		docker compose up -d $(containers); \
	else \
		echo "starting containers"; \
		docker compose up -d; \
	fi;

stop: ## Stop the app
	docker compose stop

logs: ## Display a realtime log
	docker compose logs -f

update: ## Upgrade containers
	docker compose build --no-cache
	docker compose pull

clean: stop ## Remove app containers
	docker container rm -f $(containers)
