#!/bin/bash

. .env

DB_FILE=
while true ; do
    read -r -p "Enter path to the SQL dump file you want to import : " DB_FILE
    if [ -f "$DB_FILE" ] ; then
        break
    fi
    echo "$DB_FILE is not a file..."
done

if [ -d "db/${MYSQL_DATABASE}" ]; then
    while true; do
    read -p "${MYSQL_DATABASE} database already exists. Do you want to overwrite it ? [y/N]" response
    case $response in
        [Yy]* ) docker exec -i ${APP_NAME}_db mysql -e "DROP DATABASE IF EXISTS $MYSQL_DATABASE;"; break;;
        * ) echo "Importation cancelled."; exit;;
    esac
done
fi

docker exec -i ${APP_NAME}_db mysql -e "CREATE DATABASE $MYSQL_DATABASE;
CREATE USER IF NOT EXISTS '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';
GRANT ALL ON $MYSQL_DATABASE.* TO '$MYSQL_USER'@'%';
FLUSH PRIVILEGES;"

if command -v pv &> /dev/null
then
	pv $DB_FILE | docker exec -i ${APP_NAME}_db mysql -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE
else
	docker exec -i ${APP_NAME}_db mysql -u$MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < $DB_FILE
fi

echo "Done importing ${MYSQL_DATABASE} database !";
