#!/bin/bash

. .env

if [ -d "db/${MYSQL_DATABASE}" ]; then
    while true; do
    read -p "${MYSQL_DATABASE} database already exists. Do you want to overwrite it ? [y/N]" response
    case $response in
        [Yy]* ) docker exec -i ${APP_NAME}_db mysql -e "DROP DATABASE IF EXISTS $MYSQL_DATABASE;"; break;;
        * ) echo "Creation cancelled."; exit;;
    esac
done
fi

docker exec -i ${APP_NAME}_db mysql -e "CREATE DATABASE $MYSQL_DATABASE;
CREATE USER IF NOT EXISTS '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';
GRANT ALL ON $MYSQL_DATABASE.* TO '$MYSQL_USER'@'%';
FLUSH PRIVILEGES;"

echo "Done creating ${MYSQL_DATABASE} database !";
