#!/bin/bash

RANDOM_STRING=$(openssl rand -hex 12)

echo "Type the name of your application : "
read APP_NAME

PS3='What PHP version do you want to use : '
options=("7.3" "7.4" "8.2")
select opt in "${options[@]}"
do
    case $opt in
        "7.3")
            DEBIAN_VERSION="buster"
            echo "PHP 7.3 on Debian Buster"
            break
            ;;
        "7.4")
            DEBIAN_VERSION="bullseye"
            echo "PHP 7.4 on Debian Bullseye"
            break
            ;;
        "8.2")
            DEBIAN_VERSION="bookworm"
            echo "PHP 8.2 on Debian Bookworm"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

sed -e "s/\$APP_NAME/${APP_NAME}/g" docker-compose.yml.tpl > docker-compose.yml

sed -e "s/\$APP_NAME/${APP_NAME}/g" -e "s/\$RANDOM_STRING/${RANDOM_STRING}/g" .env.tpl > .env

sed -e "s/\$DEBIAN_VERSION/${DEBIAN_VERSION}/g" Dockerfile.tpl > Dockerfile
