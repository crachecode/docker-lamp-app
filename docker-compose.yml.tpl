services:
  $APP_NAME:
    build: .
    container_name: $APP_NAME
    depends_on:
      - $APP_NAME_db
    volumes:
      - ./app:/app
      - ./config/httpd.conf:/etc/apache2/conf-enabled/90-custom.conf
      - ./config/php.ini:/etc/php/7.3/apache2/conf.d/90-custom.ini
    environment:
      - TZ
      - PUID
      - PGID
      - DB_HOST=$APP_NAME_db
      - DB_NAME=${MYSQL_DATABASE}
      - DB_USER=${MYSQL_USER}
      - DB_PASSWORD=${MYSQL_PASSWORD}
    labels:
      - "traefik.http.routers.$APP_NAME.rule=Host(`$APP_NAME.localhost`)"
    networks:
      - web
    restart: unless-stopped

  $APP_NAME_db:
    image: mariadb:10.5
    container_name: $APP_NAME_db
    volumes:
      - ./db:/var/lib/mysql
    environment:
      - TZ
      - MYSQL_DATABASE
      - MYSQL_USER
      - MYSQL_PASSWORD
      - MYSQL_ALLOW_EMPTY_PASSWORD=true
    networks:
      - web
    restart: unless-stopped

  $APP_NAME_adm:
    image: adminer
    container_name: $APP_NAME_adm
    labels:
      - "traefik.http.routers.$APP_NAME_adm.rule=Host(`$APP_NAME-adm.localhost`)"
    networks:
      - web
    restart: unless-stopped

  traefik:
    image: traefik:2.4
    container_name: traefik
    command: --api.insecure=true --providers.docker
    ports:
      - 80:80
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    labels:
      - "traefik.http.services.traefik.loadbalancer.server.port=8080"
      - "traefik.http.routers.api.rule=Host(`traefik.localhost`)"
      - "traefik.http.routers.api.service=api@internal"
    networks:
      - web
    restart: unless-stopped
  
networks:
  web:
    external: true
