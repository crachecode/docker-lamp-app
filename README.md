# Docker LAMP App

Docker LAMP App is a script that build a Docker App for any LAMP application.

It is based on Debian with Apache and mod_php.

## Disclaimer

> :warning: Only for **development** purpose !

> Ensure port 80 is available.

## Guideline

### Install Docker and Docker Compose

- [Docker](https://docs.docker.com/install/#supported-platforms)
- [Docker Compose](https://docs.docker.com/compose/install/#install-as-a-container)

### Clone this repository

```sh
git clone https://gitlab.com/crachecode/docker-lamp-app.git
```

### Run install script

```sh
cd docker-lamp-app
make install
```
Answer a few questions and place your PHP application in the `app` directory.

### Create or import a database

Optional : you can get a nice progress bar showing your database import by installing [Pipe Viewer](http://www.ivarch.com/programs/pv.shtml) on your system.

Then manage you database :

```sh
make database
```
A user will be created with the database name, and permissions on the database.

Set your PHP app config up to connect it to the database container using  `your_app_name_db` as hostname, and `your_app_name` as both database name and user. Check `.env` file to get the password.

Your application will be made avalaible at [http://your_app_name.localhost](http://your_app_name.localhost).

You can access [Adminer](https://www.adminer.org) on [http://your_app_name-adm.localhost](http://your_app_name-adm.localhost).

### Other commands

Type `make help` to get the list of all available commands.
